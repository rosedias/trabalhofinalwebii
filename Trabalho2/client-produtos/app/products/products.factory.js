app.factory("productsFactory", function($http){
 
    var factory = {}; 

    factory.readProducts = function(){
        return $http({
            method: 'GET',
            url: 'http://localhost/api/product/read.php'
        });
    };
     

factory.createProduct = function($scope){
    return $http({
        method: 'POST',
        data: {
            'name' : $scope.name,
            'description' : $scope.description,
            'price' : $scope.price,
            'category_id' : 1
        },
        url: 'http://localhost/api/product/create.php'
    });
};

factory.readOneProduct = function(id){
    return $http({
        method: 'GET',
        url: 'http://localhost/api/product/read_one.php?id=' + id
    });
};

factory.updateProduct = function($scope){
 
    return $http({
        method: 'POST',
        data: {
            'id' : $scope.id,
            'name' : $scope.name,
            'description' : $scope.description,
            'price' : $scope.price,
            'category_id' : 1
        },
        url: 'http://localhost/api/product/update.php'
    });
};

factory.deleteProduct = function(id){
    return $http({
        method: 'POST',
        data: { 'id' : id },
        url: 'http://localhost/api/product/delete.php'
    });
};

factory.searchProducts = function(keywords){
    return $http({
        method: 'GET',
        url: 'http://localhost/api/product/search.php?s=' + keywords
    });
};
     
    return factory;
});