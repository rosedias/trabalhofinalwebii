app.controller('productsController', function($scope, $mdDialog, $mdToast, productsFactory){

    // ler produtos
    $scope.readProducts = function(){

        // usar factory
        productsFactory.readProducts().then(function successCallback(response){
            $scope.products = response.data.records;
        }, function errorCallback(response){
            $scope.showToast("Unable to read record.");
        });
 
    }

    // criar produto caixa dialogo
$scope.showCreateProductForm = function(event){ 
    $mdDialog.show({
        controller: DialogController,
        templateUrl: './app/products/create_product.template.html',
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        fullscreen: true 
    });
}
 
// criar um novo produto
$scope.createProduct = function(){ 
    productsFactory.createProduct($scope).then(function successCallback(response){ 
        // novo produto criado
        $scope.showToast(response.data.message); 
        // refresh lista
        $scope.readProducts(); 
        // fecha caixa
        $scope.cancel(); 
        // remove form values
        $scope.clearProductForm(); 
    }, function errorCallback(response){
        $scope.showToast("Impossivel criar.");
    });
}
 

$scope.readOneProduct = function(id){ 
    productsFactory.readOneProduct(id).then(function successCallback(response){ 
        //atualiza o valor formulario
        $scope.id = response.data.id;
        $scope.name = response.data.name;
        $scope.description = response.data.description;
        $scope.price = response.data.price;
 
        $mdDialog.show({
            controller: DialogController,
            templateUrl: './app/products/read_one_product.template.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true,
            fullscreen: true
        }).then(
            function(){},
            // click cancelar
            function() {
                // clear modal 
                $scope.clearProductForm();
            }
        );
 
    }, function errorCallback(response){
        $scope.showToast("Não é possível recuperar o registro!!");
    });
 
}
 

$scope.showUpdateProductForm = function(id){ 
    // get editar produto
    productsFactory.readOneProduct(id).then(function successCallback(response){ 
        // atualiza valor
        $scope.id = response.data.id;
        $scope.name = response.data.name;
        $scope.description = response.data.description;
        $scope.price = response.data.price;
 
        $mdDialog.show({
            controller: DialogController,
            templateUrl: './app/products/update_product.template.html',
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true,
            fullscreen: true
        }).then(
            function(){},
 
            // usar cancelar
            function() {
                // clear modal 
                $scope.clearProductForm();
            }
        );
 
    }, function errorCallback(response){
        $scope.showToast("Unable to retrieve record.");
    });
 
}
 
$scope.updateProduct = function(){ 
    productsFactory.updateProduct($scope).then(function successCallback(response){
        $scope.showToast(response.data.message); 
        // refresh lista
        $scope.readProducts(); 
        // close dialog
        $scope.cancel(); 
        // clear modal 
        $scope.clearProductForm();
 
    },
    function errorCallback(response) {
        $scope.showToast("Não é possivel atualizar o produto!!");
    });
 
}
 
 //inicio da caixa de dialogo

$scope.confirmDeleteProduct = function(event, id){ 
    // definir id de registro para excluir
    $scope.id = id; 
    //configurações de diálogo
    var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('Product will be deleted.')
        .targetEvent(event)
        .ok('Yes')
        .cancel('No'); 
    // scaixa dialogo
    $mdDialog.show(confirm).then(
        // 'Yes' botao
        function() {
            // se o usuário clicar em "Sim", exclua o registro do produto
            $scope.deleteProduct();
        },
 
        // 'Nao' botao
        function() {
            // fim da caixa
        }
    );
}

// delete product
$scope.deleteProduct = function(){ 
    productsFactory.deleteProduct($scope.id).then(function successCallback(response){ 
        // tell the user product was deleted
        $scope.showToast(response.data.message); 
        // refresh the list
        $scope.readProducts(); 
    }, function errorCallback(response){
        $scope.showToast("Unable to delete record.");
    });
 
}

$scope.searchProducts = function(){
    productsFactory.searchProducts($scope.product_search_keywords).then(function successCallback(response){
        $scope.products = response.data.records;
    }, function errorCallback(response){
        $scope.showToast("Unable to read record.");
    });
}


$scope.clearProductForm = function(){
    $scope.id = "";
    $scope.name = "";
    $scope.description = "";
    $scope.price = "";
}

$scope.showToast = function(message){
    $mdToast.show(
        $mdToast.simple()
            .textContent(message)
            .hideDelay(3000)
            .position("top right")
    );
}

function DialogController($scope, $mdDialog) {
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
}
});