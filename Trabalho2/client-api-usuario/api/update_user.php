<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
include_once 'config/database.php';
include_once 'objects/user.php';
 

$database = new Database();
$db = $database->getConnection();

$user = new User($db);
 

//obter dados
$data = json_decode(file_get_contents("php://input"));
 
// obter jwt
$jwt=isset($data->jwt) ? $data->jwt : "";
 
// se jwt não está vazio
if($jwt){
 
    //se decodificar, mostrar detalhes do usuário
    try {
 
        // decodificar jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
 
        // definir valores
$user->firstname = $data->firstname;
$user->lastname = $data->lastname;
$user->email = $data->email;
$user->password = $data->password;
$user->id = $decoded->data->id;
 

// criar 
if($user->update()){

		// re-gerar jwt porque os detalhes do usuário podem ser diferentes
$token = array(
   "iss" => $iss,
   "aud" => $aud,
   "iat" => $iat,
   "nbf" => $nbf,
   "data" => array(
       "id" => $user->id,
       "firstname" => $user->firstname,
       "lastname" => $user->lastname,
       "email" => $user->email
   )
);
$jwt = JWT::encode($token, $key);
 
// response
http_response_code(200);
 
// response json format
echo json_encode(
        array(
            "message" => "Usuario Atualizado!",
            "jwt" => $jwt
        )
    );
}
 
// mensagem se não conseguir atualizar o usuário
else{
    //response
    http_response_code(401);
 
    // mensagem de erro
    echo json_encode(array("message" => "Não é possível atualizar o usuário."));
}
    }
 
    // se a decodificação falhar, significa que jwt é inválido
catch (Exception $e){
 
    // response
    http_response_code(401);
 
    // error message
    echo json_encode(array(
        "message" => "Accesso Negado.",
        "error" => $e->getMessage()
    ));
}
}
 
// mostrará a mensagem de erro se o jwt estiver vazio
else{
 
    //response
    http_response_code(401);
 
    // acesso negado
    echo json_encode(array("message" => "Accesso Negado!!"));
}
?>