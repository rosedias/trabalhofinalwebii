<?php
// objeto usuario
class User{

    private $conn;
    private $table_name = "users";

    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;

    public function __construct($db){
        $this->conn = $db;
    }

    // criar novo registro de usuário
function create(){
 
    //query
    $query = "INSERT INTO " . $this->table_name . "
            SET
                firstname = :firstname,
                lastname = :lastname,
                email = :email,
                password = :password";
 
    $stmt = $this->conn->prepare($query);

    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->password=htmlspecialchars(strip_tags($this->password));
 
    // ligar os valores
    $stmt->bindParam(':firstname', $this->firstname);
    $stmt->bindParam(':lastname', $this->lastname);
    $stmt->bindParam(':email', $this->email);
 
    //Hash a senha antes de salvar no banco de dados
    $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password_hash);
 
    // executo a consulta, verifico também se a consulta foi bem-sucedida
    if($stmt->execute()){
        return true;
    }
 
    return false;
}
 
// verifique se existe um email no banco
function emailExists(){
 
    // consulta para verificar se o email existe
    $query = "SELECT id, firstname, lastname, password
            FROM " . $this->table_name . "
            WHERE email = ?
            LIMIT 0,1";
 
    // preparar a consulta
    $stmt = $this->conn->prepare( $query );
 
    $this->email=htmlspecialchars(strip_tags($this->email));
 
    $stmt->bindParam(1, $this->email);

    $stmt->execute();
 
    // obtenho numero de linhas 
    $num = $stmt->rowCount();
 
    // se o email existir, atribua valores às propriedades do objeto para facilitar o acesso e uso para sessões php
    if($num>0){
 
        // obter detalhes / valores do registro
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
        // Atribuir valores às propriedades do objeto
        $this->id = $row['id'];
        $this->firstname = $row['firstname'];
        $this->lastname = $row['lastname'];
        $this->password = $row['password'];
 
        // retorno do true porque o email existe no banco de dados
        return true;
    }
 
    // alse se o email não existir no banco de dados
    return false;
}
 

// atualizar um registro do usuário
public function update(){
 
    // se a senha precisa ser atualizada
    $password_set=!empty($this->password) ? ", password = :password" : "";
 
    // se não houver senha, não atualize a senha
    $query = "UPDATE " . $this->table_name . "
            SET
                firstname = :firstname,
                lastname = :lastname,
                email = :email
                {$password_set}
            WHERE id = :id";
 
    //query
    $stmt = $this->conn->prepare($query);
 
    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
 
    // ligar os valores do formulário
    $stmt->bindParam(':firstname', $this->firstname);
    $stmt->bindParam(':lastname', $this->lastname);
    $stmt->bindParam(':email', $this->email);
 
    // Hash a senha antes de salvar no banco de dados
    if(!empty($this->password)){
        $this->password=htmlspecialchars(strip_tags($this->password));
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindParam(':password', $password_hash);
    }
 
    //ID exclusivo do registro a ser editado
    $stmt->bindParam(':id', $this->id);
 
    // executa query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}
}