<?php
header("Access-Control-Allow-Origin: http://localhost/api-users/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once 'config/database.php';
include_once 'objects/user.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);
 
// verifique a existência de e-mail aqui

$data = json_decode(file_get_contents("php://input"));

//defino valores
$user->email = $data->email;
$email_exists = $user->emailExists();

// gerar token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// verifique se o email existe e se a senha está correta
if($email_exists && password_verify($data->password, $user->password)){
 
    $token = array(
       "iss" => $iss,
       "aud" => $aud,
       "iat" => $iat,
       "nbf" => $nbf,
       "data" => array(
           "id" => $user->id,
           "firstname" => $user->firstname,
           "lastname" => $user->lastname,
           "email" => $user->email
       )
    );
 
    //response
    http_response_code(200);
 
    // gera jwt
    $jwt = JWT::encode($token, $key);
    echo json_encode(
            array(
                "message" => "Login com Sucesso!!",
                "jwt" => $jwt
            )
        );
 
}

//Falha na autenticação
else{
 
    //response
    http_response_code(401);
 
    // login falhou
    echo json_encode(array("message" => "Login falhou!!"));
}
?>