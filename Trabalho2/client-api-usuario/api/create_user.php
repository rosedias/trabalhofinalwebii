<?php
header("Access-Control-Allow-Origin: http://localhost/api-users/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once 'config/database.php';
include_once 'objects/user.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);
 
// obter dados
$data = json_decode(file_get_contents("php://input"));
 
// defino valores
$user->firstname = $data->firstname;
$user->lastname = $data->lastname;
$user->email = $data->email;
$user->password = $data->password;
 
// criar usuario
if($user->create()){
 
    //response
    http_response_code(200);
 
    // mensagem de exibição
    echo json_encode(array("message" => "Usuario Criado"));
}
 
//mensagem se não for possível criar usuário
else{
 
    //response
    http_response_code(400);
 
    // não é possível criar usuário
    echo json_encode(array("message" => "Nao é possivel criar o usuário!!"));
}
?>