<?php
header("Access-Control-Allow-Origin: http://localhost/api-users/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// decode jwt
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// recupera jwt
$data = json_decode(file_get_contents("php://input"));
 
// obter jwt
$jwt=isset($data->jwt) ? $data->jwt : "";
 
// se jwt não está vazio
if($jwt){
 
    // se decodificar, mostrar detalhes do usuário
    try {
        //decodificar jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
 
        // definir código de resposta
        http_response_code(200);
 
        //mostrar detalhes do usuário
        echo json_encode(array(
            "message" => "Accesso Garantido",
            "data" => $decoded->data
        ));
 
    }

    // se a decodificação falhar, significa que jwt é inválido
catch (Exception $e){
 
    //response
    http_response_code(401);
 
    // diga ao usuário acesso negado e mostre a mensagem de erro
    echo json_encode(array(
        "message" => "Accesso Negado!!",
        "error" => $e->getMessage()
    ));
}
}

// mostrará a mensagem de erro se o jwt estiver vazio
else{
 
    // response
    http_response_code(401);
 
    // usuário negado
    echo json_encode(array("message" => "Accesso Negado"));
}
?>